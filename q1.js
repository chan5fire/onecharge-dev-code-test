document.getElementById("button-1").onclick = () => {
    let outputStr = "";
    let inputVal = document.getElementById("question-1").value;
    if (isNaN(parseInt(inputVal))) {
        outputStr = "The number you enter is not a number";
    } else {
        for (let y = 0; y <= inputVal; y++) {
            for (let x = 0; x < inputVal; x++) {
                if (x + y >= inputVal) {
                    outputStr += "#";
                } else {
                    outputStr += "&nbsp; ";
                }
            }
            outputStr += "<br/>";
        }
    }

    document.getElementById("answer-1").innerHTML = outputStr;
}
/**
 * I spend about 5 mins to solve this problem.
 */