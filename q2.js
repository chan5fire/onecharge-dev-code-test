document.getElementById("button-2").onclick = () => {
    let outputStr = "";
    let inputVal = document.getElementById("question-2").value;
    let inputArr;
    try {
        inputArr = JSON.parse(inputVal);
        let outArr = [];
        let middleNum = Math.floor(inputArr.length / 2);

        if (inputArr.length == 2) {
            outArr.push(inputArr[middleNum]);
        } else if (inputArr.length == 1) {
            outArr = inputArr;
        } else {
            outArr = inputArr.slice(middleNum);
        }
        outputStr = "Output list: [" + outArr.toString() + "]";

    } catch (syntaxError) {
        outputStr = "You enter invalid JSON."
    }
    document.getElementById("answer-2").innerHTML = outputStr;
}
/**
 * I spend about 30 minutes to solve this problem.
 */