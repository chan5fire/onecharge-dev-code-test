document.getElementById("button-3").onclick = () => {
    let outputStr = "";
    let inputVal1 = document.getElementById("question-3-1").value;
    let inputVal2 = document.getElementById("question-3-2").value;
    let inputArr1 = JSON.parse(inputVal1);
    let inputArr2 = JSON.parse(inputVal2);
    let tempArr1 = inputArr1;
    let tempArr2 = inputArr2;

    while (tempArr1.length > 2 && tempArr2.length > 2) {
        let middleNum1 = Math.floor(tempArr1.length / 2);
        let middleNum2 = Math.floor(tempArr2.length / 2);

        if (tempArr1[middleNum1] < tempArr2[middleNum2]) {
            tempArr1 = tempArr1.slice(middleNum1);
            tempArr2 = tempArr2.slice(0, middleNum2 + 1);
        } else {
            tempArr1 = tempArr1.slice(0, middleNum1 + 1);
            tempArr2 = tempArr2.slice(middleNum2);
        }
    }

    let smallerArr;
    let largerArr;
    let median;
    if (tempArr1.length > tempArr2.length) {
        smallerArr = tempArr2;
        largerArr = tempArr1;
    } else {
        smallerArr = tempArr1;
        largerArr = tempArr2;
    }

    if (smallerArr.length == 0) {
        median = largerArr[Math.floor(largerArr.length / 2)];
    } else if (smallerArr.length == 1) {
        if (largerArr.length == 1) {
            median = (smallerArr[0] + largerArr[0]) / 2;
        } else {
            if (largerArr.length % 2 == 0) {
                let temp = [smallerArr[0],
                largerArr[largerArr.length / 2],
                largerArr[(largerArr.length / 2) - 1]];
                temp.sort((a, b) => {
                    return a - b;
                });
                median = temp[Math.floor(temp.length / 2)];
            } else {
                let largerMiddle = Math.floor(largerArr.length / 2);
                let temp = [smallerArr[0],
                largerArr[largerMiddle],
                largerArr[largerMiddle - 1],
                largerArr[largerMiddle + 1]];
                temp.sort((a, b) => {
                    return a - b;
                });

                median = (temp[(temp.length / 2) - 1] + temp[(temp.length / 2)]) / 2;
            }
        }
    } else {
        if (largerArr.length == 2) {
            let temp = smallerArr.concat(largerArr);
            temp.sort((a, b) => {
                return a - b;
            });
            median = (temp[(temp.length / 2) - 1] + temp[(temp.length / 2)]) / 2;
        } else {
            if (largerArr.length % 2 == 0) {
                let temp = [];
                temp.push(largerArr[largerArr.length / 2]);
                temp.push(largerArr[(largerArr.length / 2) - 1]);
                temp.push(Math.max(smallerArr[0], largerArr[(largerArr.length / 2) - 2]));
                temp.push(Math.min(smallerArr[1], largerArr[(largerArr.length / 2) + 1]));
                temp.sort((a, b) => {
                    return a - b;
                });
                median = (temp[(temp.length / 2) - 1] + temp[(temp.length / 2)]) / 2;

            } else {
                let largerMiddle = Math.floor(largerArr.length / 2);
                let temp = [largerArr[largerMiddle],
                Math.max(smallerArr[0], largerArr[largerMiddle - 1]),
                Math.min(smallerArr[1], largerArr[largerMiddle + 1])];
                temp.sort((a, b) => {
                    return a - b;
                });
                median = temp[Math.floor(temp.length / 2)];

            }
        }
    }

    outputStr = "The median is " + median;
    document.getElementById("answer-3").innerHTML = outputStr;
}

/**
 * I had used about 6 hours to complete this question. First I can only think of merging 2 arrays and sort them
 * ascendingly and find the median of the merged array. But it is too slow. Then I try to apply method
 * similar to binary search to eliminate the number of element but I have no idea which part should be
 * eliminate and what to do when there is only 1 or 2 element left in the array.
 * Then I search the problem on the Internet and find the solution. The final solution is reference the
 * algorithm provided in the website https://www.geeksforgeeks.org/median-of-two-sorted-arrays-of-different-sizes/
 */